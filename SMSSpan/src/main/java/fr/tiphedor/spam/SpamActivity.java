package fr.tiphedor.spam;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SpamActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_spam);
		
		Button btnPickContact = (Button) findViewById(R.id.btnContact);
		btnPickContact.setOnClickListener(btnContact);
		Button btnSend = (Button) findViewById(R.id.btnSend);
		btnSend.setOnClickListener(btnSendClicked);
	}
	static final int PICK_CONTACT_REQUEST = 1;  
	View.OnClickListener btnContact = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
		    Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
		    pickContactIntent.setType(Phone.CONTENT_TYPE); 
		    startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
		}
	};
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (requestCode == PICK_CONTACT_REQUEST) {
	        if (resultCode == RESULT_OK) {
	            Uri contactUri = data.getData();
	            String[] projection = {Phone.NUMBER};
	            Cursor cursor = getContentResolver().query(contactUri, projection, null, null, null);
	            cursor.moveToFirst();
	            int column = cursor.getColumnIndex(Phone.NUMBER);
	            String number = cursor.getString(column);
	            TextView txtNumber = (TextView) findViewById(R.id.txtPhoneNumber);
	            txtNumber.setText(number);
	        }
	    }
	}
	View.OnClickListener btnSendClicked = new View.OnClickListener() {
		public void onClick(View v) {
			final TextView txtNo = (TextView) findViewById(R.id.txtPhoneNumber);
			final TextView txtMsg = (TextView) findViewById(R.id.txtMsg);
	    	final SMSClass sms = new SMSClass();
	    	TextView txtNumber = (TextView) findViewById(R.id.txtNumber);
	    	final int nbHint = Integer.parseInt(txtNumber.getText().toString());
	    	Thread thread = new Thread()
	    	{
	    	    @Override
	    	    public void run() {
	    	    	int progress = 0;
	    	    	while (progress < nbHint) {
			    		sms.setphoneNb(txtNo.getText().toString());
			    		sms.setmsg(txtMsg.getText().toString());
			    		sms.send();
			    		progress++;
			    	}
	    	    }
	    	};
	    	thread.start();
	    }
	};
}
