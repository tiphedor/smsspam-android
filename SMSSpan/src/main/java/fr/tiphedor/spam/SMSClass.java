package fr.tiphedor.spam;

import android.telephony.SmsManager;

public class SMSClass {
	
	private String phoneNb;
	private String msg;
	private int nbEnvois;
	
	SMSClass()
	{
		this.phoneNb = "";
		this.nbEnvois = 0;
		this.msg = "";
	}
	
	public String getPhoneNb() {
	   return this.phoneNb; 
	}
	public void setphoneNb (String phoneNb) { 
	   this.phoneNb = phoneNb;
	}
	public String getMsg() {
	   return this.msg; 
	}
	public void setmsg (String msg) { 
	   this.msg = msg;
	}
	public int getNbEnvois() {
	   return this.nbEnvois; 
	}
	public void setnbEnvois (int nbEnvois) { 
	   this.nbEnvois = nbEnvois;
	}
	
	public void send()
	{
		final SmsManager smsManager = SmsManager.getDefault();
		smsManager.sendTextMessage(this.phoneNb, null, this.msg, null, null);
	}
}
